Mini test applications to learn about Flask, SQL, html and CSS.

Below Environment variables needs to be set before running any test app-

	#flask app .py file name
	export FLASK_APP=test_flask.py

	#Connection details for PostgresDB
	export DATABASE_URL="postgresql://postgres:fakePassword@localhost"

#Start Postgresql cmd
sudo -u postgres psql postgres
