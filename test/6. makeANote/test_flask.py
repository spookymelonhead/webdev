import datetime
from flask import Flask, render_template, request

app=Flask(__name__, template_folder="../templates/")
notes=[]

#make a note
@app.route("/makeanote", methods=["GET","POST"])
def makeanote():
	headline="Make a note."
	note= request.form.get("note")
	notes.append(note)
	print(notes)
	# return render_template("index_s.html", headline=headline, note_list=notes)
	return render_template("makeanote.html", headline=headline, note_list=notes)

