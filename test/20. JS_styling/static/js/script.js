document.addEventListener('DOMContentLoaded', setup);

function setup()
{
	document.querySelector("#blue").onclick = styleBlue;
	document.querySelector("#green").onclick = styleGreen;
	document.querySelector("#red").onclick = styleRed;

//Both work the same

	// document.querySelectorAll('.color-change').forEach(function(button){
	// button.onclick=function(){document.querySelector("#batnan").style.color=button.dataset.color;};		
	// });

	//	Simplifying complex looking thingy
	document.querySelectorAll('.color-change').forEach(function(button){
	button.onclick = function(){styleButton(button)};		
	});

	document.querySelector('#color-change-id').onchange= function() {
		document.querySelector("#bathman").style.color= this.value;
	};
}
function styleBlue()
{
	document.querySelector("#batman").style.color= 'blue';		
}

function styleGreen()
{
	document.querySelector("#batman").style.color= 'green';		
}

function styleRed()
{
	document.querySelector("#batman").style.color= 'red';		
}

function styleButton(button)
{
	document.querySelector("#batnan").style.color=button.dataset.color;		
}