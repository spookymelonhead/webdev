import datetime
from flask import Flask, render_template

app=Flask(__name__, template_folder="templates/")

#passing headline to the html
@app.route("/")
def index():
	title="Testing JS"
	headline="Testing JavaScript"
	return render_template("index.html", headline=headline)