import os

from flask import Flask, render_template, request
from models import *
import csv

app= Flask(__name__)
app.config["SQLALCHEMY_DATABASE_URI"]=os.getenv("DATABASE_URL")
app.config["SQLALCHEMY_TRACK_MODIFICATIONS"]=False
db.init_app(app)

def main():
    db.create_all()
    file=open("flights.csv")
    reader= csv.reader(file)
    
    #Insert data from csv
    for origin, destination, duration in reader:
        flight=Flights(origin=origin, destination=destination, duration=duration)
        db.session.add(flight)
    db.session.commit()

    #fetch all flights
    flights= Flights.query.all()
    for flight in flights:
        print ("O: %s, D:%s, Dur:%d" %(flight.origin, flight.destination, flight.duration))

    #Filter
    print(Flights.query.filter_by(origin="Delhi").count())
    
    #update existing entry
    flight=Flights.query.get(9)
    if None == flight:
        print ("Entery not found")
    else:
        flight.duration=3
        db.session.commit()

    flights= Flights.query.order_by(Flights.origin).all()
    for flight in flights:
        print ("O: %s, D:%s, Dur:%d" %(flight.origin, flight.destination, flight.duration))

    print()
    flights=Flights.query.filter(Flights.origin != "Alaska").all()
    if None == flights:
        print ("Entery not found")
    else:
        for flight in flights:
            print ("O: %s, D:%s, Dur:%d" %(flight.origin, flight.destination, flight.duration))

    print()
    flights=Flights.query.filter(Flights.origin.in_(["Alaska", "Madagascar"])).all()
    if None == flights:
        print ("Entery not found")
    else:
        for flight in flights:
            print ("O: %s, D:%s, Dur:%d" %(flight.origin, flight.destination, flight.duration))

if __name__ == "__main__":
    with app.app_context():
        main()