import datetime
from flask import Flask, render_template, request, jsonify
import requests, os


app=Flask(__name__, template_folder="templates/")

link_list=['page1', 'page2', 'page3']

#passing headline to the html
@app.route("/")
def index():
	headline='myBlog'
	pgHeadline="This is Index"
	return render_template("page1.html", headline=headline, pgHeadline= pgHeadline, link_list=link_list)

@app.route("/page1")
def page1():
	headline='myBlog'
	pgHeadline="This is Page 1"
	return render_template("page1.html", headline=headline, pgHeadline= pgHeadline, link_list=link_list)

#passing headline to the html
@app.route("/page2")
def page2():
	headline='myBlog'
	pgHeadline="This is Page 2"
	return render_template("page2.html", headline=headline, pgHeadline= pgHeadline, link_list=link_list)

#passing headline to the html
@app.route("/page3")
def page3():
	headline='myBlog'
	pgHeadline="This is Page 3"
	return render_template("page3.html", headline=headline, pgHeadline= pgHeadline, link_list=link_list)