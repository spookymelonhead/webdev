document.addEventListener('DOMContentLoaded', init);

function init()
{
	console.log('init');
	loadMyPage('page1');

	document.querySelectorAll('.go-to-link').forEach(link => {
		link.onclick = () => {
			name=link.dataset.page;
			console.log(name);
			loadMyPage(name);
			return false;
		};
	});

	//Browser Window event, if back button is pressed and current page is poped
	window.onpopstate = e => {
		console.log('Pop state');
		const data = e.state;
		document.title= data.title;
		document.querySelector("#page-data").innerHTML = data.text;
	};
}


function loadMyPage(name)
{
	//AJAX- Asynchronous Javascript and XML Communication  
	const request= new XMLHttpRequest();
	request.open('GET', `/${name}`);
	request.onload = () => {
		const response = request.responseText;
		document.querySelector("#page-data").innerHTML= response;
		document.title = name;
		/*
		history.pushState(null, name, name);
		if given null as argument state/data wouldnt be stored and if press back button
		URL would update but content wont.
		*/
		history.pushState({"name": name, "text" : response}, name, name);
	}
	request.send();
}
