import datetime
from flask import Flask, render_template, request, jsonify
import requests, os


app=Flask(__name__, template_folder="templates/")

link_list=['page1', 'page2', 'page3']

#passing headline to the html
@app.route("/")
def index():
	headline='myBlog'
	pgHeadline="Single Page website, test app"
	return render_template("index.html", headline=headline, pgHeadline= pgHeadline)

pageText=["Batman walks into a bulbasaur. Almost stumbling by the bench, he asks the bulbasaur to pour him some whiskey.<br> Bourbon he said. ", 
		" Batman walks into a chihahua. Almost stumbling by the bench, he asks the chihahua to pour him some whiskey.<br> Scotch he said. ", 
		" Batman walks into a donut. Almost stumbling by the bench, he asks the donut to pour him some whiskey.<br> Tennessee he said. "]

@app.route("/page1")
def page1():
	return pageText[0]

#passing headline to the html
@app.route("/page2")
def page2():
	return pageText[1]
#passing headline to the html
@app.route("/page3")
def page3():
	return pageText[2]