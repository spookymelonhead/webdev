document.addEventListener('DOMContentLoaded', setup);

function setup()
{
	document.querySelector('#task-submit-button').disabled = true;
	document.querySelector('#new-task').onkeyup = () => {
		if (document.querySelector('#new-task').value.length > 0)
		{
			document.querySelector('#task-submit-button').disabled = false;
		}
		else
		{
			document.querySelector('#task-submit-button').disabled = true;
		}
	}
	document.querySelector('#task-submit-button').onclick = () => {
		//create a new element on the page
		const li = document.createElement('li');
		li.innerHTML = document.querySelector('#new-task').value;

		//append
		document.querySelector('#task-list').append(li);
		//clear
		document.querySelector('#new-task').value='';
		document.querySelector('#task-submit-button').disabled = true;
		//why return false?
		return false;
	};
}
