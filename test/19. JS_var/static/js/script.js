document.addEventListener('DOMContentLoaded', setup);
const message="This is Jackass.!"

function setup()
{			
	document.querySelector("#btn4").onclick = increment;
	document.querySelector("#form").onsubmit = onFormSubmit;
}
function onFormSubmit()
{
	const name = document.querySelector("#usrname").value
	alert(`Hello ${name}`)	
}
function Hello()
{
	alert('Hello!');
}
function Hello2()
{
	document.querySelector('h1').innerHTML=message;
}
// let works only under current scope
let counter = 0;
function increment()
{
	let jack="LemonTart"
	counter++;
	if((counter % 10) === 0)
	{
		// It'd not work ${counter}
		// alert("Counter reached ${counter} Resetting the counter.");
		// Jaascript requires `` not ('' or "")

		alert(`Counter reached ${counter} Resetting the counter.`);
		counter=0;
	}
	document.querySelector('#counter').innerHTML=counter;	
}
