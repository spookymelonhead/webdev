import datetime
from flask import Flask, render_template, request, session
from flask_session import Session

app=Flask(__name__, template_folder="../templates/")

app.config["SESSION_PERMANENT"] = False
app.config["SESSION_TYPE"] = "filesystem"
Session(app)

notes=[]

#make a note
@app.route("/makeanote", methods=["GET","POST"])
def makeanote():
	if session["notes"] == None:
		session["notes"]=[]
	headline="Make a note."
	note= request.form.get("note")
	session["notes"].append(note)
	print(session["notes"])
	return render_template("makeanote.html", headline=headline, note_list=session["notes"])
