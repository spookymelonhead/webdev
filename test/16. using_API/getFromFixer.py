import requests
import os

API_BASE_URL= "http://data.fixer.io/api/latest?access_key="
#export API_ACCESS_KEY="YOUR KEY"
API_ACCESS_KEY= os.getenv("API_ACCESS_KEY")

def main():
	print("Requesting Fixer.io")
	# print(API_URL + API_ACCESS_KEY)
	response=requests.get(API_BASE_URL + API_ACCESS_KEY)
	if response.status_code == 200:
		response=response.json()
		print ("1 EUR equals to %f USD" %response["rates"]["USD"])
	else:
		raise Exception("Error: API request unsuccessfull.!")
	print("END")

if __name__ == "__main__":
	main()