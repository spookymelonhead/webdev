document.addEventListener('DOMContentLoaded', setup);

function setup()
{
	console.log('Test');
	var socket = io.connect(location.protocol + "//" + document.domain + ":" + location.port);

	socket.on('connect', ()=> {
		document.querySelectorAll('.voting-btn').forEach(button =>{
			button.onclick = () => {
				const selection = button.dataset.vote;
				socket.emit('submit vote', {"selection": selection});
			};
		});
	});

	socket.on('announce vote', data => {
		console.log(`Vote: ${data.selection}`)
		const li = document.createElement('li');
		li.innerHTML = `Vote recorded: ${data.selection}`;
		document.querySelector('#vote-list').append(li);
		document.querySelector('#harrison').innerHTML=data.votes.harrison;
		document.querySelector('#mccartney').innerHTML=data.votes.mccartney;
		document.querySelector('#lennon').innerHTML=data.votes.lennon;
		document.querySelector('#ringo').innerHTML=data.votes.ringo;
	});
}

