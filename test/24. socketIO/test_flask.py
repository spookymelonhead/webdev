import datetime
from flask import Flask, render_template, request, jsonify
import requests, os
from flask_socketio import SocketIO, emit


app=Flask(__name__, template_folder="templates/")

#export SECRET_KEY="YOUR KEY"
# app.config("SECRET_KEY")= os.getenv("SECRET_KEY")
socketio= SocketIO(app)
votes={"harrison":0, "mccartney":0, "lennon":0, "ringo":0}

#passing headline to the html
@app.route("/")
def index():
	headline1="SocketIO voting test app"
	return render_template("index.html", headline1=headline1, votes=votes)

@socketio.on("submit vote")
def vote(data):
	selection = data["selection"]
	votes[selection] = votes[selection] + 1	
	print("Selection: %s" %selection)
	emit("announce vote", {"selection": selection, "votes": votes}, broadcast= True)