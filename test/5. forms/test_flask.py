import datetime
from flask import Flask, render_template, request

app=Flask(__name__, template_folder="../templates/")

#passing headline to the html
@app.route("/")
def index():
	headline="Please enter your name"
	return render_template("index_f.html", headline=headline)

#passing a list to html
@app.route("/hello", methods=["GET","POST"])
def hello():
	if request.method == "GET":
		return("Please submit form.")
	else:	
		name= request.form.get("Name")
		print(name)
	return render_template("hello.html", name=name)

