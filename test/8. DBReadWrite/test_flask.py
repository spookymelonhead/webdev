import os
from sqlalchemy import create_engine
from sqlalchemy.orm import scoped_session, sessionmaker
import csv
#Run below on command on terminal if runnin db on Localhost
#export DATABASE_URL="postgresql://postgres:fakePassword@localhost"
engine= create_engine(os.getenv("DATABASE_URL"))
db=scoped_session(sessionmaker(bind=engine))

def main():
	flights= db.execute("SELECT * FROM flights").fetchall();
	for flight in flights:
		print("%s to %s takes %shours" %(flight.origin, flight.destination, flight.duration))

	print()

	file= open("contacts.csv")
	reader= csv.reader(file)
	for Name, Phone in reader:
		print("Inserted %s with no %s" %(Name, Phone))
		#couldn't type number directly into string , workaround
		#string data to be put under single quotes
		query="INSERT INTO myContacts (name, %s) VALUES ('%s', %s)" %("number", Name, Phone)
		userName="lol"
		password="lol"
		name="lol"
		query= "INSERT INTO userdata (username, password, name) VALUES ('%s', '%s', '%s')" %(userName, password, name)
		# print(query)
		db.execute(query);
	db.commit()

if __name__ == "__main__":
	main()