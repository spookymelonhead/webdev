import os
from sqlalchemy import create_engine
from sqlalchemy.orm import scoped_session, sessionmaker
import csv

#what it'd do?
# Displays FLights table
# Takes input from user for Flight id
# Displays passengers for the given flight ID

#Run below on command on terminal if runnin db on Localhost
#export DATABASE_URL="postgresql://postgres:fakePassword@localhost"
engine= create_engine(os.getenv("DATABASE_URL"))
db=scoped_session(sessionmaker(bind=engine))

def main():
	flights= db.execute("SELECT * FROM flights").fetchall();
	for flight in flights:
		print("ID: %d O:%s to D:%s takes T:%shours" %(flight.id, flight.origin, flight.destination, flight.duration))

	print()
	flightId=int(input("Please Enter the Flight Id"));

	qResult=db.execute("SELECT * FROM flights WHERE id=%d" %flightId).fetchone();
	if qResult is None:
		print("No Flight with Id: %d found" %flightId)
		return
	passengers=db.execute("SELECT * FROM passengers WHERE flight_id=%d" %flightId).fetchall();

	for passenger in passengers:
		print("Name: %s" %(passenger.name))


if __name__ == "__main__":
	main()