console.log('Start')
let counter = 1;
const quantity = 20;


document.addEventListener('DOMContentLoaded', load);
window.onscroll = () => {
	if(window.innerHeight + window.scrollY >= document.body.offsetHeight)
	{
		console.log('Inner height: ');
		console.log(window.innerHeight);
		console.log('window.scrollY');
		console.log(window.scrollY);
		console.log('document.body.offsetHeight');
		console.log(document.body.offsetHeight);		
		console.log('load');
		load();
	}
};
function load()
{
	const start = counter;
	const end=  start +  quantity - 1;
	counter = end + 1;

	//start a request
	const request = new XMLHttpRequest();
	request.open('POST', '/posts');
	request.onload = () => {
		const data = JSON.parse(request.responseText);
		data.forEach(addPost);
	}

	const data = new FormData();
	data.append('start', start);
	data.append('end', end);
	request.send(data);
}


function addPost(contents)
{	
	console.log('added post')
	const post = document.createElement("div");
	post.className = "post"
	post.innerHTML= contents;

	const hidebtn=document.createElement("button");
	hidebtn.innerHTML='hide';
	hidebtn.className='hide-btn'
	post.append(hidebtn);

	hidebtn.onclick = function(){
		this.parentElement.remove();
	}

	document.querySelector("#page-data").append(post);
}