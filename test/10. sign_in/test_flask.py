from flask import Flask, render_template, request

import os
from sqlalchemy import create_engine
from sqlalchemy.orm import scoped_session, sessionmaker

import datetime

app=Flask(__name__, template_folder="templates/")

#Run below on command on terminal if runnin db on Localhost
#export DATABASE_URL="postgresql://postgres:fakePassword@localhost"
engine= create_engine(os.getenv("DATABASE_URL"))
db=scoped_session(sessionmaker(bind=engine))

#passing headline to the html
@app.route("/", methods=["GET","POST"])
def index():
	pageTitle="SignIn"
	heading="Welcome to my blog, please sign in."
	return render_template("index.html", pageTitle=pageTitle, heading=heading)

@app.route("/home", methods=["GET","POST"])
def home():
	if request.method == "GET":
		return "Please submit via form, Get has no support."
	elif request.method == "POST":	
		userName= request.form.get("userName")
		password= request.form.get("password")
		name= 	  request.form.get("name")
		print("Received usrname: %s, passwd: %s, name: %s" %(userName, password, name))

		#check if username already exists in system
		qResult= db.execute("SELECT * FROM  userdata WHERE username='%s'" %(userName)).fetchone()
		print(qResult)
		if qResult == None:
			success = True
			print("Inserting new user into DB")
			qResult= db.execute("INSERT INTO userdata (username, password, name) VALUES ('%s', '%s', '%s')" %(userName, password, name))
			db.commit()
			heading="Sign In success."
			pageTitle="Home"
			return render_template("home.html", heading=heading, pageTitle=pageTitle, success=success)
		else:
			#return to index
			success= False;
			heading="Sorry! Username already taken."
			pageTitle="SignIn"
			return render_template("index.html", heading=heading, pageTitle=pageTitle, success=success)
	else:
		return "Unsupported http method used, please contact admin."