from flask import Flask, render_template, request, jsonify

import os
from models import *

app=Flask(__name__, template_folder="templates/")

#Run below on command on terminal if runnin db on Localhost
#export DATABASE_URL="postgresql://postgres:fakePassword@localhost"
app.config["SQLALCHEMY_DATABASE_URI"]=os.getenv("DATABASE_URL")
app.config["SQLALCHEMY_TRACK_MODIFICATIONS"]=False
db.init_app(app)

#passing headline to the html 
@app.route("/", methods=["GET","POST"])
def index():
	pageTitle="Book a Flight"
	heading="Please select the Flight below."
	flights= Flights.query.all()
	return render_template("index.html", pageTitle=pageTitle, heading=heading, flights=flights)

@app.route("/home", methods=["GET","POST"])
def home():
	if request.method == "GET":
		return "Please submit via form, Get has no support."
	elif request.method == "POST":	
		flightId= request.form.get("flightid")
		name= request.form.get("name")
		print("Received FLightId: %s, Name: %s" %(flightId, name))

		#check if username already exists in system
		success = True
		print("Inserting new user into DB")
		passenger = Passengers(name=name, flight_id=flightId)
		db.session.add(passenger)
		db.session.commit()
		heading="Booking Confirmed..!"
		pageTitle="Home"
		return render_template("home.html", heading=heading, pageTitle=pageTitle, success=success)
	else:
		return "Unsupported http method used, please contact admin."

@app.route("/api/flights/<int:flight_id>")
def flights_api(flight_id):
	flight= Flights.query.get(flight_id)
	if flight is None:
		print()
		print()
		print('Flight ID')
		print(flight_id)
		return jsonify({"error": "invalid_flight_id"})
	else:
		passenger_list=[]
		passengers= Passengers.query.filter_by(flight_id=flight_id)
		if passengers is None:
			print()
		else:
			for passenger in passengers:
				passenger_list.append(passenger.name)
		return jsonify({
						"origin": flight.origin,
						"destination": flight.destination,
						"duration": flight.duration,
						"passengers": passenger_list
						})
