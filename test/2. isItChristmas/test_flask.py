import datetime
from flask import Flask, render_template

app=Flask(__name__, template_folder="../templates/")

#passing binary flag to html, if else cases handled on html side
@app.route("/isitchristmas")
def isitchristmas():
	now=datetime.datetime.now()
	if 12 == now.month and 25 == now.day :
		isitchristmas=True
	else:
		isitchristmas=False
		isitchristmas=True
	return render_template("index.html", headline="", isitchristmas=isitchristmas)