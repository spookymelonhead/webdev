import datetime
from flask import Flask, render_template, request, jsonify
import requests, os, time


app=Flask(__name__, template_folder="templates/")


#passing headline to the html
@app.route("/")
def index():
	headline='myBlog'
	pgHeadline="Inifinite Scroll or dynamically loadigng page content test app"
	return render_template("index.html", headline=headline, pgHeadline= pgHeadline)

@app.route("/posts", methods = ["POST"])
def posts():
	headline='myBlog'
	pgHeadline="Inifinite Scroll or dynamically loadigng page content test app"

	start= int(request.form.get("start"))
	end= int(request.form.get("end"))

	print("start %d end %d" %(start, end))
	data=[]
	for num in range(start, end + 1):
		data.append("%d Mississippi" %num)

	time.sleep(1)
	print(jsonify(data))
	return jsonify(data)	