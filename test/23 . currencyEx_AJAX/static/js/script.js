document.addEventListener('DOMContentLoaded', setup);

function setup()
{
	console.log('Test');
	document.querySelector("#currency-submit-btn").onclick = conversion;
}

function conversion()
{
	const request = new XMLHttpRequest();
	const currency = document.querySelector("#currency").value;
	//send request
	request.open('POST', '/convert', true);

	console.log('Test2');
	// when the request is Done
	request.onload = () =>{
		console.log('Test3');
		const data = JSON.parse(request.responseText);
		if(data.success)
		{
			console.log('Success');
			document.querySelector("#result").innerHTML = data.rate;	
		}
		else
		{
			console.log('Failure');
			document.querySelector("#result").innerHTML = "There was an Error.";
		}
	};

	const data = new FormData();
	data.append('currency', currency);
	//send request
	request.send(data);
	console.log('Request sent.')
	return false;
}

