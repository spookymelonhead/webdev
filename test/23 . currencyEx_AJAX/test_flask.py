import datetime
from flask import Flask, render_template, request, jsonify
import requests, os


app=Flask(__name__, template_folder="templates/")

API_BASE_URL= "http://data.fixer.io/api/latest?access_key="
#export API_ACCESS_KEY="YOUR KEY"
API_ACCESS_KEY= os.getenv("API_ACCESS_KEY")

#passing headline to the html
@app.route("/")
def index():
	headline1="Currency conversion using Fixer.io api and AJAX"
	return render_template("index.html", headline1=headline1)

@app.route("/convert", methods=["POST"])
def convert():
	currency= request.form.get("currency")
	print("Requesting fixer.io")
	res= requests.get(API_BASE_URL + API_ACCESS_KEY)
	
	if res.status_code != 200:
		print("Call to API was FAIL.!")
		return jsonify({"success": False})
	else:
		data= res.json()
		if currency not in data["rates"]:
			return jsonify({"success": False})	
		else:
			return jsonify({"success": True, "rate": data["rates"][currency]}) 