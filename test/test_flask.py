import datetime
from flask import Flask, render_template

app=Flask(__name__, template_folder="templates/")

#passing headline to the html
@app.route("/")
def index():
	headline="This is headline dynamically generated via passing variable from flask python to html"
	return render_template("index_l.html", headline=headline, isitchristmas=False)

@app.route("/brian")
def brian():
	return ("Hello Brian..!")

@app.route("/<string:name>")
def hello(name):
	return "<h1>Hello %s..!</h1>" %name

@app.route("/bye")
def bye():
	headline="This is Goodbye."
	return render_template("index.html", headline=headline)

#passing binary flag to html, if else cases handled on html side
@app.route("/isitchristmas")
def isitchristmas():
	now=datetime.datetime.now()
	if 12 == now.month and 25 == now.day :
		isitchristmas=True
	else:
		isitchristmas=False
	return render_template("index.html", headline="", isitchristmas=isitchristmas)

#passing a list to html
@app.route("/names")
def names():
	name_list= ["Anna", "Brian", "Catie"]
	return render_template("index.html", headline="", name_list=name_list)

#passing a list to html
@app.route("/more")
def more():
	return render_template("more_l.html")

