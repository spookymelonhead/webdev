import datetime
from flask import Flask, render_template

app=Flask(__name__, template_folder="../templates/")

#passing headline to the html
@app.route("/")
def index():
	headline="This is headline dynamically generated via passing variable from flask python to html"
	return render_template("index.html", headline=headline, isitchristmas=False)

#passing a list to html
@app.route("/names")
def names():
	name_list= ["Anna", "Brian", "Catie"]
	return render_template("index.html", headline="", name_list=name_list)

#passing a list to html
@app.route("/more")
def more():
	return render_template("more.html")

