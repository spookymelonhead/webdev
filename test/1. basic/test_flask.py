import datetime
from flask import Flask, render_template

app=Flask(__name__)

#passing headline to the html
@app.route("/")
def index():
	myString="<h1 style=\"text-align: center\">Hello world..!</h1>"
	return myString

@app.route("/brian")
def brian():
	return ("Hello Brian, you moron!")

@app.route("/<string:name>")
def hello(name):
	return "<h1>Hello %s..!</h1>" %name