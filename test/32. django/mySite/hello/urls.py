from django.urls import path
from . import views


urlpatterns = [
	# empty path, and func to be call to render the page content 
	path("hello", views.index)
]