from django.shortcuts import render
from django.http import HttpResponse, Http404, HttpResponseRedirect
from django.urls import reverse
from django.contrib.auth import login, logout, authenticate

# Create your views here.
def index(request):
	if not request.user.is_authenticated:
		return render(request, 'login/login.html', {"message": None})
	else:
		context = {
			"title": "Welcome",
			"user": request.user,
		}
		return render(request, 'login/user.html', context)

def user(request):
	username= request.POST["username"]
	password= request.POST["password"]
	print(username)
	print(password)
	user=authenticate(request, username=username, password=password)
	if user is not None:
		login(request, user)
		return HttpResponseRedirect(reverse("index"))
	else:
		return render(request, 'login/login.html', {"message": "Invalid credentials"})

def logoutview(request):
	logout(request)
	if request.user.is_authenticated:
		print('Did not log out.')
		return HttpResponse("Did not logout successfully.!")	
	return render(request, 'login/login.html', {"message": "Logged Out"})