from django.urls import path
from . import views


urlpatterns = [
	# empty path, and func to be call to render the page content 
	path("", views.index, name='index'),
	path("<int:flightId>", views.flight, name='flight'),
	path("<int:flightId>/bookaflight", views.bookaflight, name='bookaflight'),
]