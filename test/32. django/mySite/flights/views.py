from django.shortcuts import render
from django.http import HttpResponse, Http404, HttpResponseRedirect
from django.urls import reverse
from .models import Flight, Airport, Passenger

# Create your views here.
def index(request):
	#dictionary
	context ={
		"airports": Airport.objects.all(),
		"flights": Flight.objects.all(),
		"title": "listing flights airports"
	}
	return render(request, "flights/index.html", context)

def flight(request, flightId):
	print("FlightId passed is: %d" %flightId)
	try:
		flight = Flight.objects.get(pk=flightId)
		passengers= flight.passengers.all()
		nonPassengers= Passenger.objects.exclude(flights=flight)

	except Flight.DoesNotExist:
		raise Http404("Flight Does Not Exist")
	context ={
		"flight": flight,
		"passengers": passengers,
		"nonPassengers": nonPassengers,
		"title": "FlightId in URL",
		"flightId": flightId,
	}
	return render(request, "flights/flight.html", context)

def bookaflight(request, flightId):
	try:
		passengerId= int(request.POST["passenger_id"])
		passenger= Passenger.objects.get(pk=passengerId)
		flight= Flight.objects.get(pk=flightId)

	except Passenger.DoesNotExist:
		return render(request, "flights/error.html", {'message': "Passenger not found!"})	
	except Flight.DoesNotExist:
		return render(request, "flights/error.html", {'message': "Flight not found!"})	
	except KeyError:
		return render(request, "flights/error.html", {'message': "KeyError not found!"})

	passenger.flights.add(flight)
	
	return HttpResponseRedirect(reverse("flight", args=(flightId,)))
