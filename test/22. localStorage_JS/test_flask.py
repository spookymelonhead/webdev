import datetime
from flask import Flask, render_template

app=Flask(__name__, template_folder="templates/")

#passing headline to the html
@app.route("/")
def index():
	headline1="Counter increments by itself"
	headline2="Local storage"
	return render_template("index.html", headline1=headline1, headline2=headline2)