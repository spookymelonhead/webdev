document.addEventListener('DOMContentLoaded', setup);

function setup()
{
	let counter = 0;
	setInterval(increment, 1000);
	if(!localStorage.getItem('counter'))
	{
		localStorage.setItem('counter', 0);
		alert(`counter not found`);
	}
	else
	{
		counter = localStorage.getItem('counter')
		counter++;
	}
	document.querySelector("#count1").innerHTML= counter;
	document.querySelector("#count2").innerHTML= counter;

	function increment()
	{
		counter++;
		document.querySelector("#count1").innerHTML= counter;
		localStorage.setItem('counter', counter);
	}
}
