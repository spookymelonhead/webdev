from flask import Flask, render_template, request

import os
from sqlalchemy import create_engine
from sqlalchemy.orm import scoped_session, sessionmaker

import datetime

app=Flask(__name__, template_folder="templates/")

#Run below on command on terminal if runnin db on Localhost
#export DATABASE_URL="postgresql://postgres:fakePassword@localhost"
engine= create_engine(os.getenv("DATABASE_URL"))
db=scoped_session(sessionmaker(bind=engine))

#passing headline to the html 
@app.route("/", methods=["GET","POST"])
def index():
	pageTitle="Book a Flight"
	heading="Please select the Flight below."
	flights= db.execute("SELECT * FROM flights").fetchall()
	return render_template("index.html", pageTitle=pageTitle, heading=heading, flights=flights)

@app.route("/home", methods=["GET","POST"])
def home():
	if request.method == "GET":
		return "Please submit via form, Get has no support."
	elif request.method == "POST":	
		flightId= request.form.get("flightid")
		name= request.form.get("name")
		print("Received FLightId: %s, Name: %s" %(flightId, name))

		#check if username already exists in system
		success = True
		print("Inserting new user into DB")
		qResult= db.execute("INSERT INTO passengers (name, flight_id) VALUES (:name, :flight_id)", 
			{"name": name, "flight_id": flightId})
		db.commit()
		heading="Booking Confirmed..!"
		pageTitle="Home"
		return render_template("home.html", heading=heading, pageTitle=pageTitle, success=success)
	else:
		return "Unsupported http method used, please contact admin."